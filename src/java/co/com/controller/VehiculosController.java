    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.controller;

import co.com.model.Categoria;
import co.com.model.Datospersonales;
import co.com.model.Estadovehiculo;
import co.com.model.Rol;
import co.com.model.Usuario;
import co.com.model.Usuariorol;
import co.com.model.Vehiculo;
import com.sun.faces.facelets.util.Path;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.IOException;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
/**
 *
 * @author ENVY 15
 */
@ManagedBean(name="vehiculo")
@ViewScoped
public class VehiculosController {
    
      @Resource
      private UserTransaction ut;
     @PersistenceContext
    private EntityManager em;
     
     private List<Vehiculo> listVehiculo= new ArrayList<Vehiculo>();
     
     private List<Categoria> listCategoria= new ArrayList<Categoria>();
     
     private List<Estadovehiculo> listEstado= new ArrayList<Estadovehiculo>();
     
     private Datospersonales detalle;
     
     private Vehiculo newVehiculo = new Vehiculo();
     
     private int idCategoria;
     
     private int idEstado;
     
     private int idFiltro;
     
     private int idVendedor;
     
      private int precioDesde;
     
     private int precioHasta;
     

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

     
    public Datospersonales getDetalle() {
        return detalle;
    }

    public void setDetalle(Datospersonales detalle) {
        this.detalle = detalle;
    }
     
     

    public int getPrecioDesde() {
        return precioDesde;
    }

    public void setPrecioDesde(int precioDesde) {
        this.precioDesde = precioDesde;
    }

    public int getPrecioHasta() {
        return precioHasta;
    }

    public void setPrecioHasta(int precioHasta) {
        this.precioHasta = precioHasta;
    }

    public int getIdFiltro() {
        return idFiltro;
    }

    public void setIdFiltro(int idFiltro) {
        this.idFiltro = idFiltro;
    }
     
     

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }    
     

    public Vehiculo getNewVehiculo() {
        return newVehiculo;
    }

    public void setNewVehiculo(Vehiculo newVehiculo) {
        this.newVehiculo = newVehiculo;
    }
     
     

    public List<Categoria> getListCategoria() {
        return listCategoria;
    }

    public void setListCategoria(List<Categoria> listCategoria) {
        this.listCategoria = listCategoria;
    }

    public List<Estadovehiculo> getListEstado() {
        return listEstado;
    }

    public void setListEstado(List<Estadovehiculo> listEstado) {
        this.listEstado = listEstado;
    }
     
     

    public List<Vehiculo> getListVehiculo() {
        return listVehiculo;
    }

    public void setListVehiculo(List<Vehiculo> listVehiculo) {
        this.listVehiculo = listVehiculo;
    }

 
         
    
     public VehiculosController(){     
      
         }
     
     public void listarPrecio(){
         
     listVehiculo=new ArrayList<Vehiculo>();
     listVehiculo= em.createNativeQuery("select * from vehiculo where Precio BETWEEN "+precioDesde+" and "+precioHasta+"", Vehiculo.class).getResultList();
     
     }
         
    public void listar (){     
         try {
             
             if (idFiltro !=0) {
                 System.out.println("id categoria "+idFiltro);
                 listVehiculo=new ArrayList<Vehiculo>();
                 listVehiculo= em.createNativeQuery("Select * from vehiculo where Id_Categoria="+idFiltro+" ", Vehiculo.class).getResultList();               
                 
             }else{
             listVehiculo= em.createNamedQuery("Vehiculo.findAll").getResultList();
             //System.out.println(listVehiculo.size());
             }
             for (Vehiculo vehiculo : listVehiculo) {
                 System.out.println("id vendedor "+vehiculo.getIdVendedor().getCorreo());
             }
             
         } catch (Exception e) {
             System.out.println(e.getMessage());
         }                 
     }

     public List<Categoria> listarCategoria (){     
         try {
             listCategoria = em.createNamedQuery("Categoria.findAll").getResultList();
            // System.out.println(listCategoria.size());
         } catch (Exception e) {
             System.out.println(e.getMessage());
         }
          return listCategoria;
         
     }
     
     public List<Estadovehiculo> listarEstado (){     
         try {
             listEstado = em.createNamedQuery("Estadovehiculo.findAll").getResultList();
             System.out.println(listEstado.size());
         } catch (Exception e) {
             System.out.println(e.getMessage());
         }
          return listEstado;
         
     }
     
      public List<Datospersonales> listarVendedor (){     
          List<Datospersonales> listDatos = new ArrayList<Datospersonales>();
         try {             
             listDatos = em.createNativeQuery("select dp.* from datospersonales dp "
                     + " INNER JOIN usuario u on dp.Id_Usuario=u.idUsuario "
                     + " INNER JOIN usuariorol ur on u.idUsuario=ur.idUsuario "
                     + " where ur.idRol=1", Datospersonales.class).getResultList();
             System.out.println(listDatos.size());
         } catch (Exception e) {
             System.out.println(e.getMessage());
         }
          return listDatos;         
     }
     
     public void AgregarVehiculo () throws NotSupportedException, SystemException, RollbackException, HeuristicMixedException, HeuristicRollbackException, IOException{     
     
         
         newVehiculo.setIdCategoria((Categoria) em.createNamedQuery("Categoria.findByIdCategoria").setParameter("idCategoria", idCategoria).getSingleResult());
         newVehiculo.setIdEstado((Estadovehiculo) em.createNamedQuery("Estadovehiculo.findByIdEstado").setParameter("idEstado", idEstado).getSingleResult());
         newVehiculo.setIdVendedor((Datospersonales) em.createNamedQuery("Datospersonales.findById").setParameter("id", idVendedor).getSingleResult());
         
         
         System.out.println(newVehiculo.getIdCategoria().getDescripcion());
         System.out.println(newVehiculo.getIdEstado().getDescripcion());
         System.out.println(newVehiculo.getIdVendedor().getNombre());
         
         ut.begin();
         em.persist(newVehiculo);
         ut.commit();
         
               ExternalContext ec = FacesContext.getCurrentInstance()
        .getExternalContext();         
            
            ec.redirect(ec.getRequestContextPath()
            + "/faces/listVehiculos.xhtml");
         
         
     }
     
     public void VerDetalle (Datospersonales var) {     
     
         detalle = new Datospersonales();
         
        detalle=var;
         
         System.out.println(detalle.getCorreo());
         
     }
    
    
}
