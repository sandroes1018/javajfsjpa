/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.controller;
import co.com.model.Categoria;
import co.com.model.Datospersonales;
import co.com.model.Estadovehiculo;
import co.com.model.Rol;
import co.com.model.Usuario;
import co.com.model.Usuariorol;
import co.com.model.Vehiculo;
import com.sun.faces.facelets.util.Path;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

@ManagedBean(name="index")
@ViewScoped
public class IndexBean {
    
     @Resource
      private UserTransaction ut;
     @PersistenceContext
    private EntityManager em;
     
    private String user;
    private String pass;
    private String msg="";    
    private static List<Usuariorol> rol= new ArrayList<Usuariorol>();
    private static  Datospersonales persona;   

    public  Datospersonales getPersona() {
        return persona;
    }

    public  void setPersona(Datospersonales persona) {
        IndexBean.persona = persona;
    }

    
    public String getMsg() {
        return msg;
    }



    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
       public List<Usuariorol> getRol() {
        return rol;
    }

    public void setRol(List<Usuariorol> rol) {
        this.rol = rol;
    }
     
    
    public void login(){
        
        Usuario userLogin = new Usuario();
        
        try{
            
        userLogin= (Usuario) em.createNamedQuery("Usuario.login").setParameter("u", user).setParameter("c", pass).getSingleResult();
        rol=userLogin.getUsuariorolList();
        
        persona=(Datospersonales) em.createNativeQuery("Select * from DatosPersonales where id_Usuario="+userLogin.getIdUsuario()+"", Datospersonales.class).getSingleResult();
        
            ExternalContext ec = FacesContext.getCurrentInstance()
        .getExternalContext();         
            
            ec.redirect(ec.getRequestContextPath()
            + "/faces/panel.xhtml");
            
            for (Usuariorol usuariorol : rol) {
                System.out.println(usuariorol.getIdRol().getDescripcion());
            }
            
            System.out.println(persona.getApellido());
            
        }catch(Exception e){
            
         System.out.println("incorrecto"+e.getMessage());
         msg="Usuario Incorrecto";
         
        }
        
    }
    
    public void EditPersona() throws NotSupportedException, SystemException, RollbackException, HeuristicMixedException, HeuristicRollbackException, IOException{
        
        Datospersonales edit= new Datospersonales();
        
        edit=(Datospersonales) em.createNamedQuery("Datospersonales.findById").setParameter("id", persona.getId()).getSingleResult();
        
        edit.setApellido(persona.getApellido());
        edit.setNombre(persona.getNombre());
        edit.setTelefono(persona.getTelefono());
        edit.setCorreo(persona.getCorreo());
        
        
        ut.begin();
        em.merge(edit);
        ut.commit();
       
         ExternalContext ec = FacesContext.getCurrentInstance()
        .getExternalContext();         
            
            ec.redirect(ec.getRequestContextPath()
            + "/faces/panel.xhtml");
        
    }

 
     

   
}
