/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ENVY 15
 */
@Entity
@Table(name = "estadovehiculo", catalog = "examen", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estadovehiculo.findAll", query = "SELECT e FROM Estadovehiculo e"),
    @NamedQuery(name = "Estadovehiculo.findByIdEstado", query = "SELECT e FROM Estadovehiculo e WHERE e.idEstado = :idEstado"),
    @NamedQuery(name = "Estadovehiculo.findByDescripcion", query = "SELECT e FROM Estadovehiculo e WHERE e.descripcion = :descripcion")})
public class Estadovehiculo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Estado", nullable = false)
    private Integer idEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "Descripcion", nullable = false, length = 255)
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstado")
    private List<Vehiculo> vehiculoList;

    public Estadovehiculo() {
    }

    public Estadovehiculo(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Estadovehiculo(Integer idEstado, String descripcion) {
        this.idEstado = idEstado;
        this.descripcion = descripcion;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Vehiculo> getVehiculoList() {
        return vehiculoList;
    }

    public void setVehiculoList(List<Vehiculo> vehiculoList) {
        this.vehiculoList = vehiculoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstado != null ? idEstado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estadovehiculo)) {
            return false;
        }
        Estadovehiculo other = (Estadovehiculo) object;
        if ((this.idEstado == null && other.idEstado != null) || (this.idEstado != null && !this.idEstado.equals(other.idEstado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.model.Estadovehiculo[ idEstado=" + idEstado + " ]";
    }
    
}
