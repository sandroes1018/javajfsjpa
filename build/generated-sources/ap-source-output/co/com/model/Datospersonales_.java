package co.com.model;

import co.com.model.Usuario;
import co.com.model.Vehiculo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2018-09-04T23:40:20")
@StaticMetamodel(Datospersonales.class)
public class Datospersonales_ { 

    public static volatile SingularAttribute<Datospersonales, String> apellido;
    public static volatile SingularAttribute<Datospersonales, String> correo;
    public static volatile SingularAttribute<Datospersonales, Usuario> idUsuario;
    public static volatile SingularAttribute<Datospersonales, Integer> id;
    public static volatile SingularAttribute<Datospersonales, String> telefono;
    public static volatile SingularAttribute<Datospersonales, String> nombre;
    public static volatile ListAttribute<Datospersonales, Vehiculo> vehiculoList;

}