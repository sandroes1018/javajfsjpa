package co.com.model;

import co.com.model.Rol;
import co.com.model.Usuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2018-09-04T23:40:20")
@StaticMetamodel(Usuariorol.class)
public class Usuariorol_ { 

    public static volatile SingularAttribute<Usuariorol, Rol> idRol;
    public static volatile SingularAttribute<Usuariorol, Integer> idUserRol;
    public static volatile SingularAttribute<Usuariorol, Usuario> idUsuario;

}