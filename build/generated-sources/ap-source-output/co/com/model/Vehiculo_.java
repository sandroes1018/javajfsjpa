package co.com.model;

import co.com.model.Categoria;
import co.com.model.Datospersonales;
import co.com.model.Estadovehiculo;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2018-09-04T23:40:20")
@StaticMetamodel(Vehiculo.class)
public class Vehiculo_ { 

    public static volatile SingularAttribute<Vehiculo, Double> precio;
    public static volatile SingularAttribute<Vehiculo, Estadovehiculo> idEstado;
    public static volatile SingularAttribute<Vehiculo, Integer> idVehiculo;
    public static volatile SingularAttribute<Vehiculo, String> color;
    public static volatile SingularAttribute<Vehiculo, Datospersonales> idVendedor;
    public static volatile SingularAttribute<Vehiculo, Categoria> idCategoria;
    public static volatile SingularAttribute<Vehiculo, String> modelo;

}