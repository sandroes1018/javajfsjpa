package co.com.model;

import co.com.model.Vehiculo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2018-09-04T23:40:20")
@StaticMetamodel(Estadovehiculo.class)
public class Estadovehiculo_ { 

    public static volatile SingularAttribute<Estadovehiculo, String> descripcion;
    public static volatile SingularAttribute<Estadovehiculo, Integer> idEstado;
    public static volatile ListAttribute<Estadovehiculo, Vehiculo> vehiculoList;

}